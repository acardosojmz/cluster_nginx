CREATE DATABASE dbtest CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use  dbtest;

CREATE TABLE courses 
( id BIGINT AUTO_INCREMENT
, title VARCHAR (100)
, duration INT 
, CONSTRAINT pkcourses PRIMARY KEY (id)
);

INSERT INTO courses (title, duration)
VALUES 
('Docker for dummies',20),
('Docker level expert',40),
('Kubernetes',120),
('Julia',80),
('Kotlin Flow',40); 

GRANT ALL PRIVILEGES ON dbtest.* TO h4ck3r@'%' IDENTIFIED BY 'm4x1m4s3gur1d4d';