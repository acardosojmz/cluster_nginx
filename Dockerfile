FROM php:7.4-fpm-alpine
RUN docker-php-ext-install pdo pdo_mysql mysqli opcache
RUN echo "expose_php = Off">>/usr/local/etc/php/php.ini